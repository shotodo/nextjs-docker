import Head from 'next/head'
import styles from '../styles/Home.module.css'

export default function Home() {
  return (
    <div className={styles.container}>
      <Head>
        <title>Shotodo</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <main className={styles.main}>
        <h1 className={styles.title}>
          Shotodo
        </h1>        
      </main>

      <footer className={styles.footer}>
        <a href='https://functionary.co' target='_blank' rel='noopener noreferrer'>From the house of Functionary</a>
      </footer>
    </div>
  )
}
